$ontext

Esempio di ricerca dell'albero dei cammini minimi dal nodo A verso tutti gli altri nodi
senza usare la matrice di incidenza nodi-archi.
Se si vuole il cammino minimo da A verso il nodo, ad esempio, E porre  d(i) domande /A -1,B 0,C 0,D 0,E 1/.
$Offtext

sets
   i 'nodes' /A,B,C,D,E/;

alias(i,j);
set arcs(i,j) /A.B, A.C, B.D, B.E, C.B, C.D, D.E/;

parameters
c(i,j) costi archi /A.B 3, A.C 4,B.D 2,B.E 5,C.B 1,C.D 3,D.E 6/
d(i) domande /A -4,B 1,C 1,D 1,E 1/;

variables
x(i,j),
costo 'costo totale';
integer variable x;

*x.lo(i,j)=0;



equations
costotot,
bildom(i) bilancio domanda;

*costotot..sum((arc), c(arc)*x(arc))=e=costo;
costotot..sum((i,j)$arcs(i,j), c(i,j)*x(i,j))=e=costo;
*bildom(i)..sum(j,(x(j,i)-x(i,j)))=e=d(i);
bildom(i).. sum(arcs(j,i), x(j,i)) - sum(arcs(i,j), x(i,j))=e=d(i);

model path /all/;
 solve path minimizing costo using mip;
