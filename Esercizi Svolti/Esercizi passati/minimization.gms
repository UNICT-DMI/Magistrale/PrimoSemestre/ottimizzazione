

free variable cost "total cost";

variables
x1,x2;


positive variables
x1,x2;

equations
obj  "min total cost",
constr1,
constr2,
constr3;

obj..
400*x1 + 600*x2 =e= cost;
constr1..
140*x1 =g= 70;
constr2..
20*x1+10*x2 =g= 30;
constr3..
25*x1+50*x2 =g= 75;
model totalcost /all/;
solve totalcost using lp minimizing cost;

