sets
   i 'nodes' /node1*node6/
alias(i,j);
set arcs(i,j) /node1.node2, node1.node4, node1.node5, node2.node3, node2.node4,node3.node4, node4.node5, node4.node6, node5.node6/;


parameters
c(i,j) costi archi /node1.node2 8, node1.node4 7, node1.node5 10, node2.node3 10, node2.node4 5,node3.node4 9, node4.node5 7, node4.node6 10, node5.node6 3/,
d(i) domande / node1 -5, node2 1, node3 1, node4 1, node5 1, node6 1/;




variables
x(i,j),
costo 'costo totale';

integer variable x;


equations
costotot
bildom(i) bilancio domanda;

costotot..sum((i,j)$arcs(i,j), c(i,j)*x(i,j))=e=costo;
bildom(i).. sum(arcs(j,i), x(j,i)) - sum(arcs(i,j), x(i,j))=e=d(i);
model flusso /all/;
 solve flusso minimizing costo using mip;
