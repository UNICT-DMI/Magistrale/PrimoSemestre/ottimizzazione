SETS
  I    depot             /D1, D2 /
  J    demandarea        /A1, A2/

TABLE   DIST(I,J) in kilometer

           A1         A2
D1         400        350
D2         250        550   ;

PARAMETER   DEM(J)  demand at demandarea j in tons
  / A1         40
    A2         10 / ;

PARAMETER  SUP(I)    supply in tons
  /D1      30
   D2      30 /;



VARIABLES  X(I,J)  shipment from i to j
           Z             totalcost;

POSITIVE VARIABLE X;

EQUATIONS
    SATDEM(J)  fullfillment of aggregate demand
    SATSUP(I)   availability constraint
    TOTALCOST  totalcost  ;

SATDEM(J) ..   SUM(I,X(I,J)) =G= DEM(J);
SATSUP(I)  ..   SUM(J,X(I,J)) =L= SUP(I);
TOTALCOST ..   Z  =E= SUM((I,J),DIST(I,J)*X(I,J));

MODEL TRANSP1 /ALL/;

SOLVE TRANSP1 USING LP MINIMIZING Z;
