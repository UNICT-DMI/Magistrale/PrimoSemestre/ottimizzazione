

free variable profit;
variables
x1 ,
x2;
positive variables
x1,x2;

equations
obj  "max total profit",
constr1,
constr2;

obj..
5*x1 + 20*x2 =e= profit;
constr1..
1*x1+3*x2 =l= 200;
constr2..
3*x1+2*x2 =l= 160;

model totalprofit /all/;
solve totalprofit using lp maximizing profit;

