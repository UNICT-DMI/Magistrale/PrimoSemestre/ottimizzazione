$Ontext
Variables  x11,x12,x13,x21,x22,x23,x31,x32,x33,objvar;

Integer Variables x11,x12,x13,x21,x22,x23,x31,x32,x33;

Equations  e1,e2,e3,e4,e5,e6,e7;


e1..    x11 + x12 + x13  =E= 1;

e2..    x21 + x22 + x23  =E= 1;

e3..    x31 + x32 + x33  =E= 1;

e4..    x11 + x21 + x31  =E= 1;

e5..    x12 + x22 + x32 =E= 1;

e6..    x13 + x23 + x33  =E= 1;


e7..  5*x11+4*x12+7*x13+6*x21+7*x22+3*x23+8*x31+11*x32+2*x33=e= objvar;


Model assegn / all /;

Solve assegn using mip minimizing objvar;

$Offtext

Set
I /C1,C2,C3/,
J /Lav1,Lav2,Lav3/;

Table a(I,J)

    Lav1  Lav2  Lav3
C1    5    4     7
C2    6    7     3
C3    8    11    2   ;

Variables
x(I,J),
z;

Binary variable x;

Equations
vincolo1(I),
vincolo2(J),
obj;


vincolo1(I).. sum(J,x(I,J))=e=1;
vincolo2(J).. sum(I,x(I,J))=e=1;
obj..z=e= sum((I,J),a(I,J)*x(I,J));

Model assegn / all /;

Solve assegn minimizing z using mip;