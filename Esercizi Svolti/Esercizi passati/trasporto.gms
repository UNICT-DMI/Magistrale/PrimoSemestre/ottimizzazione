 option optcr=0.0;

set I magazzini /Verona, Perugia, Roma, Pescara, Taranto, Lamezia/;
set J porti /Genova, Venezia, Ancona, Napoli, Bari/;

Parameters

b(I) disponibilitÓ container
/Verona 10
Perugia 12
Roma 20
Pescara 24
Taranto 18
Lamezia 40/,

d(J)  domanda container
/Genova 20
Venezia 15
Ancona 25
Napoli 33
Bari 21/;

Table c(I,J)    distanza

         Genova Venezia Ancona Napoli Bari
Verona     290    115     355   715    810
Perugia    380    340     165   380    610
Roma       505    530     285   220    450
Pescara    655    450     155   240    315
Taranto    1010   840     550   305    95
Lamezia    1072  1097     747   372    333  ;

Variables
x(I,J) numero di container,
z     costo totale  ;

Positive Variable x;


Equations
offerta(I),
domanda(J),
costo;

offerta(I).. sum(J,x(I,J))=l=b(I);
domanda(J).. sum(I,x(I,J))=g=d(J);
costo..z=e= sum((I,J),c(I,J)*x(I,J));

 Model trasporto /all /;
 Solve trasporto minimizing z using lp;
