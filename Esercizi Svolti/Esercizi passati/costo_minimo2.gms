$Ontext

sets
   i 'nodes' /A,B,C,D,E/
   j 'arcs' /AB, AC, BC, BE, CD, DB, DE/
;


parameters
c(j) costi archi /AB 2, AC 3,BC 1,BE 5,CD 1,DB 2,DE 4/
d(i) domande /A -5,B 6,C -5,D 0,E 4/
cap(j) capacit� /AB 6, AC 4, BC 2, BE 7,CD  8,DB 4,DE 5/  ;

table m(i,j)

  AB AC BC BE CD DB DE
A -1 -1  0  0  0  0  0
B  1  0 -1 -1  0  1  0
C  0  1  1  0 -1  0  0
D  0  0  0  0  1 -1 -1
E  0  0  0  1  0  0  1   ;


variables
x(j),
costo 'costo totale';

positive variable x;
x.up(j)=cap(j);

equations
costotot
bildom(i) bilancio domanda;

costotot..sum(j, c(j)*x(j))=e=costo;
bildom(i)..sum(j,m(i,j)*x(j))=e=d(i);

model path /all/;
 solve path minimizing costo using lp;
$Offtext



sets
   i 'nodi' /nodo1*nodo6/,
   j archi /nodo1nodo2, nodo1nodo4, nodo1nodo5, nodo2nodo3, nodo2nodo4, nodo3nodo4, nodo4nodo5, nodo4nodo6, nodo5nodo6/;

parameters

cap(j) /
   nodo1nodo2   8
   nodo1nodo4   5
   nodo1nodo5   7
   nodo2nodo3   5
   nodo2nodo4   6
   nodo3nodo4   9
   nodo4nodo5   5
   nodo4nodo6   6
   nodo5nodo6   4 /,

c(j) /
   nodo1nodo2   7
   nodo1nodo4   4
   nodo1nodo5   4
   nodo2nodo3   7
   nodo2nodo4   7
   nodo3nodo4   9
   nodo4nodo5   9
   nodo4nodo6   3
   nodo5nodo6   8 / ,


 d(i) /nodo1 -9, nodo2 -2, nodo3 -1, nodo4 3, nodo5 4, nodo6 5/ ;


table m(i,j)

      nodo1nodo2  nodo1nodo4    nodo1nodo5   nodo2nodo3  nodo2nodo4     nodo3nodo4 nodo4nodo5  nodo4nodo6 nodo5nodo6
nodo1 -1                -1          -1           0          0            0          0           0           0
nodo2  1                 0           0           -1         -1           0          0           0           0
nodo3  0                 0           0           1           0           -1         0           0           0
nodo4  0                 1           0           0           1            1         -1          -1          0
nodo5  0                 0           0           0           0            0         1           0           -1
nodo6  0                 0           1           0           0            0         0           1           1      ;

variables
x(j),
costo 'costo totale';

positive variable x;
x.up(j)=cap(j);

equations
costotot
bildom(i) bilancio domanda;

costotot..sum(j, c(j)*x(j))=e=costo;
bildom(i)..sum(j,m(i,j)*x(j))=e=d(i);

model flusso /all/;
 solve flusso minimizing costo using lp;
