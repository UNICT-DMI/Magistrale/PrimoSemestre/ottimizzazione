var x1>=0, integer;
var x2>=0, integer;
var x3>=0, integer;
var x4>=0, integer;
var x5>=0, integer;
var y1>=0, integer;
var y2>=0, integer;
var y3>=0, integer;
var y4>=0, integer;
var y5>=0, integer;
var z1>=0, integer;
var z2>=0, integer;
var z3>=0, integer;
var z4>=0, integer;
var z5>=0, integer;
minimize costo: 2000*(x1+x2+x3+x4+x5)+1000*(y1+y2+y3+y4+y5);
s.t. ore_mese1: 160*x1-50*y1>=6000;
s.t. ore_mese2: 160*x2-50*y2>=7000;
s.t. ore_mese3: 160*x3-50*y3>=8000;
s.t. ore_mese4: 160*x4-50*y4>=11000;
s.t. tecnici: x1=50;
#Si dovrebbero inserire i vincoli:
#s.t. v1 x2=0.95*x1+y1;
#s.t. v2 x3=0.95*x2+y2;
#s.t. v3 x4=0.95*x3+y3;
#s.t. v4 x4=0.95*x3+y3;
# ma sono incompatibili con l'integralità delle variabili. Si sostituisono #allora con il floor di 0.95 che viene poi linearizzato con i 
#seguenti vincoli
s.t. v1: z1+y1=x2;
s.t. v2: z2+y2=x3;
s.t. v3: z3+y3=x4;
s.t. v4: z4+y4=x5;
s.t. v5: z1<=0.95*x1;
s.t. v6: z2<=0.95*x2;
s.t. v7: z3<=0.95*x3;
s.t. v8: z4<=0.95*x4;
s.t. v9: 0.95*x1<=z1+1;
s.t. v10: 0.95*x2<=z2+1;
s.t. v11: 0.95*x3<=z3+1;
s.t. v12: 0.95*x4<=z4+1;











